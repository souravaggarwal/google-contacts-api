import requests as r
import os
import json
from rest_framework.views import APIView
from rest_framework.response import Response
from .utils import refresh_token

class ContactList(APIView):
    """ To perform all CRUD opteration on google contacts
    """
    BASE_URL = "https://people.googleapis.com/"
    
    def get(self, request):
        """ It fetches all connection contacts.

        Returns:
            A list of type dict, with persons name, phone Numbers, etag, resourceName
            Ex: {"contacts_list" : [{"resourceName": people/xx, "etag":xx, "name":xx, "Numbers":[xx]},] }
        """
        url = "{}v1/people/me/connections?personFields=names,emailAddresses,phoneNumbers".format(self.BASE_URL)
        resp = r.get(url, headers=refresh_token())
        resp_dict = json.loads(resp.content)
        final_data = []
        if "connections" in resp_dict:
            for contact in resp_dict["connections"]:
                data = {"resourceName": contact["resourceName"], "etag": contact["etag"]}
                if "names" in contact:
                    data["name"] = contact["names"][0]["displayName"]

                if "phoneNumbers" in contact:
                    number_list = []
                    for number in contact["phoneNumbers"]:
                        number_list.append(number["value"])
                    data["Numbers"] = number_list
                final_data.append(data)
            final_resp = {"contacts_list": final_data, "code": 200}
            return Response(final_resp)
        return Response(resp_dict)

    def post(self, request):
        """ It creates a new contact on google account.
        Args:
            firstName: Person's first name
            lastName: Person's last name
            contactNumber: Person's contact Number
        Returns:
            Created Contact's data like(etag, resourceName)
        """
        url = "{}v1/people:createContact".format(self.BASE_URL)
        request_data = request.data.dict()

        if set(["firstName", "lastName", "contactNumber", ]) <= set(request_data):
            payload = {"names": [{"givenName": request_data["firstName"], "familyName": request_data["lastName"]}],
                       "phoneNumbers": [{"value": request_data["contactNumber"]}]}
            resp = r.post(url, data=json.dumps(payload), headers=refresh_token())
            resp_data = json.loads(resp.content)
            return Response(resp_data)
        return Response({"error": {"message": "Invalid Data", "code": 400}})

    def delete(self, request):
        """ It deletes a contact from google account
        Args:
            resourceName: Contact's Unique Id, Format: "people/xx"
        Return:
            an error if contact doesn't exists
        """
        request_data = request.data.dict()
        if "resourceName" in request_data and request_data["resourceName"].startswith("people/"):
            resource_name = request_data["resourceName"]
            url = "{}/v1/{}:deleteContact".format(self.BASE_URL, resource_name)
            resp = r.delete(url, headers=refresh_token())
            resp_dict = json.loads(resp.content)
            if not resp_dict:
                resp_dict = {"message": "Contact deleted :)"}
            return Response(resp_dict)
        return Response({"error": {"message": "Invalid Data", "code": 400}})

    def patch(self, request):
        """ It updates a person's contact number
        Args:
            contactNumber: new Contact Number
            etag: Unique contact's tag
            resourceName: Unique contact's Id
        Returns:
            Updated contact's data
        """
        request_data = request.data.dict()
        if "resourceName" in request_data and request_data["resourceName"].startswith("people/"):
            resource_name = request_data["resourceName"]
            url = "{}v1/{}:updateContact?updatePersonFields=phoneNumbers".format(self.BASE_URL, resource_name)
            payload = {
                "resourceName": request_data["resourceName"],
                "etag": request_data["etag"],
                "phoneNumbers": [{"value": request_data["contactNumber"]}],
            }
            resp = r.patch(url, data=json.dumps(payload), headers=refresh_token())
            resp_data = json.loads(resp.content)
        return Response(resp_data)


class ContactDetails(APIView):
    BASE_URL = "https://people.googleapis.com/"
    def post(self, request):
        """ To get the complete details for a particular contact
        Args:
            resourceName: Contact's Unique Id, Format: "people/xx"
        Returns:
            Complete details of the contact.
        """
        request_data = request.data.dict()
        if (("resourceName" in request_data) and (request_data["resourceName"].startswith("people/"))):
            resource_name = request_data["resourceName"]     
            url = "{}v1/{}?personFields=names,emailAddresses".format(self.BASE_URL, resource_name)
            resp = r.get(url, headers=refresh_token())
            resp_data = json.loads(resp.content)
            return Response(resp_data)
        return Response({"error": {"message": "Invalid_Data", "code": 400}})
