from django.urls import path, re_path
from .views import ContactList, ContactDetails

urlpatterns = [
    path('contact/', ContactList.as_view()),
    path('details/', ContactDetails.as_view())
   
]