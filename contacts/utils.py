import requests as r
import os
import json
from rest_framework.views import APIView
from rest_framework.response import Response

def refresh_token():
    """ Helper method to fetch access token
    Returns:
        Authorization headers
    """
    #curr_path = os.path.abspath(os.path.dirname(__file__))
    with open('info.dat') as f:
        file_data = json.loads(f.read())
    url = "https://www.googleapis.com/oauth2/v4/token?access_type=offline"
    payload = {
        "grant_type": "refresh_token",
        "client_id": file_data["client_id"],
        "refresh_token": file_data["refresh_token"],
        "client_secret": file_data["client_secret"],
        "access_type": "offline",
    }
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    resp = r.post(url, data=payload, headers=headers)
    resp_data = json.loads(resp.content)
    head_resp = {"Authorization": "Bearer {}".format(resp_data["access_token"]),
                    "Accept": "application/json", "Content-Type": "application/json"}
    return head_resp