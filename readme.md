# google-contacts-api

## Google Account Authorization:
1) To connect this application with your google account.
    1) Run the script oauth.py
       -  `python oauth.py`
    2) Select your google account
    3) click Allow
    4) It will show a message "authentication flow has completed."

2) To change the connected google account: Delete the file "info.dat" and follow the above steps.
    


## Start the Django Server:
-python manage.py runserver


## CRUD operations on google contacts:
1) GET  http://localhost:8000/people/contact/
    - It returns a list of all the contacts.

2) POST http://localhost:8000/people/contact/
    - To Create a new Contact
    - Args:
        - firstName: Person's first name
        - lastName: Person's last name
        - contactNumber: Person's contact Number
    
3) PATCH http://localhost:8000/people/contact/
    - To update the person's contact number
    - Args:
        - contactNumber: new Contact Number
        - etag: Unique contact's tag
        - resourceName: Unique contact's Id
    
4) DELETE http://localhost:8000/people/contact/
    - To delete the contact
    - Args:
        - resourceName: Contact's Unique Id, Format: "people/xx"
        
5) POST http://localhost:8000/people/details/
    - It returns the complete details of the given contact
    - Args:
        - resourceName: Contact's Unique Id, (Format "people/xx")

