import httplib2

from oauth2client.file import Storage
from oauth2client.client import OAuth2WebServerFlow
from oauth2client.tools import run_flow
from apiclient.discovery import build

CLIENT_ID="384333440646-rspfqu2pl7rsitdhn3dk28jqv1ljibd8.apps.googleusercontent.com"
CLIENT_SECRET="03_NzrpGYbsiuJF-l3H-Rh_j"
PROJECT_ID ="firstdemo-207206"

FLOW = OAuth2WebServerFlow(
    client_id=CLIENT_ID,
    client_secret=CLIENT_SECRET,
    scope='https://www.googleapis.com/auth/contacts', 
    user_agent=PROJECT_ID)

storage = Storage('info.dat')
credentials = storage.get()
if credentials is None or credentials.invalid == True:
  credentials = run_flow(FLOW, storage)

http = httplib2.Http()
http = credentials.authorize(http)
